
This repository contains all codes employed in the article: A skew-normal dynamic linear model and Bayesian forecasting, by R. B. Arellano-Valle, J. E. Contreras-Reyes, F. O. L�pez Quintero and A. Valdebenito, 2018 (https://link.springer.com/article/10.1007/s00180-018-0848-1).

**JAGS codes:**

1. NDLM.txt
2. SNDLM.txt

**R codes for simulation:**

1. SNDLM\_0.R: the '\_0.R' suffix means the case for lambda=0. It is needed hard code the lambda parameter to 3 for emulate results under this value.
2. simulations_figures.R
3. simulations_tables.R

**R codes for real data application:**

1. Aplications.R
2. Aplications_n.R: where the '\_n' suffix means the normal model.
3. Applications_tables_figures.R


data are in data folder as same a pair of extra functions in extra directory.

Results were last proved using this library version:

* Martyn Plummer (2018). rjags: Bayesian Graphical Models using MCMC. R package version 4-8. https://CRAN.R-project.org/package=rjags
  
* R Core Team (2018). R: A language and environment for statistical
computing. R Foundation for Statistical Computing, Vienna, Austria.
URL https://www.R-project.org/. R version 3.5.1.

